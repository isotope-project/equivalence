use std::{
    collections::{HashMap, HashSet},
    fmt::Display,
};

use darling::{FromDeriveInput, FromField, FromMeta, ToTokens};
use proc_macro::{self, TokenStream};
use proc_macro2::{Ident, Span, TokenStream as TokenStream2};
use quote::quote;
use syn::{
    parse_macro_input, punctuated::Punctuated, token, Data, DeriveInput, Field, PathArguments,
};

#[derive(FromDeriveInput, Default)]
#[darling(default, attributes(equiv))]
struct EquivalenceOpts {
    #[darling(multiple)]
    rel: Vec<String>,
    fwd: Option<String>,
    partial_eq: Option<bool>,
    eq: Option<bool>,
    hash: Option<bool>,
    partial_ord: Option<bool>,
    ord: Option<bool>,
    delegate: Option<bool>,
    delegate_partial_eq: Option<bool>,
    delegate_hash: Option<bool>,
    delegate_partial_ord: Option<bool>,
    delegate_ord: Option<bool>,
}

/// The forwarding options for fields
#[derive(FromField, Default)]
#[darling(default, attributes(equiv))]
struct FieldEquivalenceOpts {
    #[darling(multiple)]
    case: Vec<Case>,
    ignore: bool,
    eq: bool,
    rel: Option<String>,
    map: Option<String>,
    cmp: Option<String>,
}

impl FieldEquivalenceOpts {
    /// Get the default forwarding destination for this relation, given the defaults for the struct
    ///
    /// Panics if these field options are invalid
    fn default_fwd_dst(&self, default_fwd: &DefaultFwd) -> Option<DefaultFwd> {
        match (self.ignore, self.eq, &self.rel, &self.map, &self.cmp) {
            (true, false, None, None, None) => None,
            (false, true, None, None, None) => Some(DefaultFwd::ToDest(FwdDest::ToPartialEq)),
            (false, false, Some(rel), None, None) => Some(DefaultFwd::ToDest(FwdDest::ToRel(
                rel.parse().expect("invalid relation type"),
            ))),
            (false, false, None, Some(map), None) => Some(DefaultFwd::ToDest(FwdDest::ToMap(
                map.parse().expect("invalid map function"),
            ))),
            (false, false, None, None, Some(cmp)) => Some(DefaultFwd::ToDest(FwdDest::ToCmp(
                cmp.parse().expect("invalid comparator function"),
            ))),
            (false, false, None, None, None) => Some(default_fwd.clone()),
            (ignore, eq, rel, map, cmp) => panic!(
                "cannot specify more than one default behaviour for a field's equivalence relation\
                \nignore = {ignore}\
                \neq = {eq}\
                \nrel = {rel:?}\
                \nmap = {map:?}\
                \ncmp = {cmp:?}"
            ),
        }
    }
}

/// An individual case for a particular relation
#[derive(FromMeta, Default)]
#[darling(default)]
struct Case {
    src: String,
    ignore: bool,
    eq: bool,
    rel: Option<String>,
    map: Option<String>,
    cmp: Option<String>,
}

impl Case {
    /// Get the forwarding destination for this case
    ///
    /// Panics if this case is invalid
    fn fwd_dst(&self) -> Option<FwdDest> {
        match (self.ignore, self.eq, &self.rel, &self.map, &self.cmp) {
            (true, false, None, None, None) => None,
            (false, true, None, None, None) => Some(FwdDest::ToPartialEq),
            (false, false, Some(rel), None, None) => {
                Some(FwdDest::ToRel(rel.parse().expect("invalid relation type")))
            }
            (false, false, None, Some(map), None) => {
                Some(FwdDest::ToMap(map.parse().expect("invalid map function")))
            }
            (false, false, None, None, Some(cmp)) => Some(FwdDest::ToCmp(
                cmp.parse().expect("invalid comparator function"),
            )),
            (false, false, None, None, None) => panic!("cannot specify empty behaviour for a case"),
            _ => panic!("cannot specify more than one behaviour for a case's equivalence relation"),
        }
    }
}

/// The default relation to forward an equivalence implementation to
#[derive(Clone)]
enum DefaultFwd {
    /// Forward `R` to `PartialEquiv<R>`
    ToSelf,
    /// Perform the given forwarding by default
    ToDest(FwdDest),
}

impl DefaultFwd {
    /// Convert this default forwarding destination to a concrete destination, given the name of the current relation
    fn to_dest(&self, relation: &TokenStream2) -> FwdDest {
        match self {
            DefaultFwd::ToSelf => FwdDest::ToRel(relation.clone()),
            DefaultFwd::ToDest(dest) => dest.clone(),
        }
    }
}

/// A destination to forward an equivalence implementation to
#[derive(Clone)]
enum FwdDest {
    /// Forward this implementation to `PartialEq`
    ToPartialEq,
    /// Forward this implementation to the given `T`
    ToRel(TokenStream2),
    /// Forward this implementation to a map
    ToMap(TokenStream2),
    /// Forward this implementation to a comparator
    ToCmp(TokenStream2),
}

impl FwdDest {
    /// Get a quotation for this equivalence implementation for a struct, given a field name
    fn into_struct_quote(self, field_name: &TokenStream2) -> TokenStream2 {
        match self {
            FwdDest::ToPartialEq => quote! { (self. #field_name == other. #field_name) },
            FwdDest::ToRel(rel) => {
                quote! { equivalence::PartialEquiv::<#rel>::equiv(&self. #field_name, &other. #field_name) }
            }
            FwdDest::ToMap(map) => {
                quote! { ((#map)(&self. #field_name) == (#map)(&other. #field_name)) }
            }
            FwdDest::ToCmp(cmp) => quote! { ((#cmp)(&self. #field_name, &other. #field_name)) },
        }
    }

    /// Get a quotation for this equivalence implementation for an enum, given field names for the left and right hand sides of the comparison
    fn into_enum_quote(self, self_name: &TokenStream2, other_name: &TokenStream2) -> TokenStream2 {
        match self {
            FwdDest::ToPartialEq => quote! { (#self_name == #other_name) },
            FwdDest::ToRel(rel) => {
                quote! { equivalence::PartialEquiv::<#rel>::equiv(#self_name, #other_name) }
            }
            FwdDest::ToMap(map) => {
                quote! { ((#map)(#self_name) == (#map)(#other_name)) }
            }
            FwdDest::ToCmp(cmp) => quote! { ((#cmp)(#self_name, #other_name)) },
        }
    }
}

fn collect_generic_dependencies<'a>(
    ty: &'a syn::Type,
    type_params: &'a HashSet<&'a proc_macro2::Ident>,
    generic_buffer: &mut Vec<&'a proc_macro2::Ident>,
) {
    match ty {
        syn::Type::Array(ar) => collect_generic_dependencies(&ar.elem, type_params, generic_buffer),
        syn::Type::Group(gr) => collect_generic_dependencies(&gr.elem, type_params, generic_buffer),
        syn::Type::Paren(pr) => collect_generic_dependencies(&pr.elem, type_params, generic_buffer),
        syn::Type::Path(tpath) => {
            for segment in tpath.path.segments.iter() {
                if type_params.contains(&segment.ident) {
                    assert!(
                        segment.arguments == PathArguments::None,
                        "internal error: higher kinded generic. Please report this crash as a bug!"
                    );
                    generic_buffer.push(&segment.ident)
                }
            }
        }
        syn::Type::Reference(rf) => {
            collect_generic_dependencies(&rf.elem, type_params, generic_buffer)
        }
        syn::Type::Slice(sl) => collect_generic_dependencies(&sl.elem, type_params, generic_buffer),
        syn::Type::Tuple(tup) => {
            for ty in tup.elems.iter() {
                collect_generic_dependencies(ty, type_params, generic_buffer)
            }
        }
        //TODO?: syn::Type::ImplTrait(_) => todo!(),
        //TODO?: syn::Type::Ptr(_) => todo!(),
        //TODO?: syn::Type::TraitObject(_) => todo!(),
        //TODO?: syn::Type::Macro(_) => todo!(),
        _ => {}
    }
}

fn field_conjunction<'a>(
    relations: &mut HashMap<String, DerivationState>,
    field_no: usize,
    field: &'a Field,
    default_fwd: &DefaultFwd,
    type_params: &'a HashSet<&'a proc_macro2::Ident>,
    generic_buffer: &mut Vec<&'a proc_macro2::Ident>,
    mut into_quote: impl FnMut(FwdDest) -> TokenStream2,
) {
    // // Collect generic dependencies in where clause4
    generic_buffer.clear();
    collect_generic_dependencies(&field.ty, type_params, generic_buffer);

    let forwards =
        FieldEquivalenceOpts::from_field(field).expect("invalid forwarding specifier on field");
    let field_default_fwd = forwards.default_fwd_dst(&default_fwd);
    for case in forwards.case.iter() {
        if case.src == "" {
            panic!("must specify relation for case")
        }
        let state = relations
        .get_mut(&case.src)
        .expect("invalid relation for case; make sure to specify all relations in the rel field of the root equiv attribute");
        if state.forwarded == true {
            panic!("already forwarded field {} for case {:?}; for a conjunction, please use an explicit comparator function", 
        if let Some(ident) = &field.ident { ident as &dyn Display } else { &field_no  as &dyn Display},
        case.src
        );
        }
        if let Some(dest) = case.fwd_dst() {
            state
                .partial_eq_expr
                .extend([token::AndAnd::default().to_token_stream(), into_quote(dest)]);
        }
        state.forwarded = true;
    }
    for (_relation, state) in relations.iter_mut() {
        if !state.forwarded {
            if let Some(field_default_fwd) = &field_default_fwd {
                state.partial_eq_expr.extend([
                    token::AndAnd::default().to_token_stream(),
                    into_quote(field_default_fwd.to_dest(&state.relation)),
                ])
            }
        }
        state.forwarded = false;
    }
}

/// PartialEquiv derivation state
struct DerivationState {
    /// A token for the current relation
    relation: TokenStream2,
    /// This derivation's set of where clauses for partial equivalence
    partial_eq_where_clause: Option<TokenStream2>,
    /// This derivation's partial equivalence expression
    partial_eq_expr: TokenStream2,
    /// Whether this relation has been forwarded or not
    forwarded: bool,
}

//TODO: take generics/trait bounds into account!

#[proc_macro_derive(PartialEquiv, attributes(equiv))]
pub fn derive_equivalence(input: TokenStream) -> TokenStream {
    let input = parse_macro_input!(input);
    let mut opts =
        EquivalenceOpts::from_derive_input(&input).expect("invalid options to #[equiv(...)]");
    if opts.rel.is_empty() {
        opts.rel.push("()".to_string());
    }
    let DeriveInput {
        ident,
        data,
        generics,
        ..
    } = input;

    let type_params: HashSet<_> = generics
        .params
        .iter()
        .filter_map(|generic| match generic {
            syn::GenericParam::Type(ty) => Some(&ty.ident),
            _ => None,
        })
        .collect();
    let mut generic_buffer = Vec::with_capacity(type_params.len());
    let (impl_generics, ty_generics, where_clause) = generics.split_for_impl();
    let where_clause = where_clause.map(|clause| clause.to_token_stream());

    let mut relations: HashMap<_, _> = opts
        .rel
        .into_iter()
        .map(|relation| {
            let tok: TokenStream2 = relation.parse().expect("invalid relation type");
            (
                relation,
                DerivationState {
                    relation: tok,
                    partial_eq_where_clause: where_clause.clone(),
                    partial_eq_expr: quote! {},
                    forwarded: false,
                },
            )
        })
        .collect();

    let default_fwd = match opts.fwd.as_deref() {
        Some("self") => DefaultFwd::ToSelf,
        None => DefaultFwd::ToDest(FwdDest::ToPartialEq),
        Some(fwd) => DefaultFwd::ToDest(FwdDest::ToRel(fwd.parse().expect("invalid forward type"))),
    };

    match data {
        Data::Struct(s) => {
            for state in relations.values_mut() {
                state.partial_eq_expr = quote! { true }
            }
            for (field_no, field) in s.fields.iter().enumerate() {
                let field_name = if let Some(ident) = &field.ident {
                    ident.to_token_stream()
                } else {
                    syn::Index::from(field_no).to_token_stream()
                };
                field_conjunction(
                    &mut relations,
                    field_no,
                    field,
                    &default_fwd,
                    &type_params,
                    &mut generic_buffer,
                    |dest| dest.into_struct_quote(&field_name),
                );
            }
        }
        Data::Enum(e) => {
            for variant in e.variants.iter() {
                let variant_name = &variant.ident;
                match &variant.fields {
                    syn::Fields::Named(_) => {
                        let self_names: Punctuated<_, token::Comma> =
                            Punctuated::from_iter(variant.fields.iter().map(|field| {
                                field
                                    .ident
                                    .as_ref()
                                    .expect("struct variants should have named fields")
                            }));
                        let other_names: Punctuated<_, token::Comma> =
                            Punctuated::from_iter(self_names.pairs().map(|pair| {
                                let field_name = pair.value();
                                let other_name = format!("{field_name}_other");
                                let mut result = TokenStream2::new();
                                result.extend([
                                    field_name.to_token_stream(),
                                    token::Colon::default().to_token_stream(),
                                    Ident::new(&*other_name, Span::call_site()).to_token_stream(),
                                ]);
                                result
                            }));
                        for state in relations.values_mut() {
                            state.partial_eq_expr.extend(quote! {
                                (
                                    #ident :: #variant_name { #self_names },
                                    #ident :: #variant_name { #other_names }
                                ) => true
                            })
                        }
                    }
                    syn::Fields::Unnamed(_) => {
                        let len = variant.fields.len();
                        let self_names: Punctuated<_, token::Comma> = Punctuated::from_iter(
                            (0..len).map(|i| Ident::new(&format!("x{i}"), Span::call_site())),
                        );
                        let other_names: Punctuated<_, token::Comma> = Punctuated::from_iter(
                            (0..len).map(|i| Ident::new(&format!("y{i}"), Span::call_site())),
                        );
                        for state in relations.values_mut() {
                            state.partial_eq_expr.extend(quote! {
                                (
                                    #ident :: #variant_name ( #self_names ),
                                    #ident :: #variant_name ( #other_names )
                                ) => true
                            })
                        }
                    }
                    syn::Fields::Unit => {
                        for state in relations.values_mut() {
                            state.partial_eq_expr.extend(
                                quote! {(#ident :: #variant_name, #ident :: #variant_name) => true},
                            )
                        }
                    }
                };
                for (field_no, field) in variant.fields.iter().enumerate() {
                    let (self_name, other_name) = if let Some(ident) = &field.ident {
                        (
                            ident.clone(),
                            Ident::new(&format!("{ident}_other"), Span::call_site()),
                        )
                    } else {
                        (
                            Ident::new(&format!("x{field_no}"), Span::call_site()),
                            Ident::new(&format!("y{field_no}"), Span::call_site()),
                        )
                    };
                    field_conjunction(
                        &mut relations,
                        field_no,
                        field,
                        &default_fwd,
                        &type_params,
                        &mut generic_buffer,
                        |dest| {
                            dest.into_enum_quote(
                                &self_name.to_token_stream(),
                                &other_name.to_token_stream(),
                            )
                        },
                    );
                }
                for state in relations.values_mut() {
                    state
                        .partial_eq_expr
                        .extend(token::Comma::default().to_token_stream())
                }
            }
            for state in relations.values_mut() {
                let expr = &state.partial_eq_expr;
                state.partial_eq_expr = quote! {
                    match (self, other) {
                        #expr
                        _ => false,
                    }
                }
            }
        }
        Data::Union(_) => panic!("the PartialEquiv derive macro does not support unions"),
    };
    let mut output = quote! {};
    for (_relation, state) in relations {
        debug_assert!(!state.forwarded);
        let relation = &state.relation;
        let expr = &state.partial_eq_expr;
        let where_clause = &state.partial_eq_where_clause;
        output.extend(quote! {
            impl #impl_generics equivalence::PartialEquiv<#ident #ty_generics, #relation> for #ident #ty_generics #where_clause {
                fn equiv(&self, other: &Self) -> bool {
                    #expr
                }
            }
        })
    }
    output.into()
}
