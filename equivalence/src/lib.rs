// extern crate self as equivalence;
// use equivalence_derive::Equivalence;

use std::{cmp::Ordering, hash::Hasher};

/// Values equipped with a partial equivalence relation specified by the type `R`
trait PartialEquiv<T = Self, R = ()> {
    /// Check whether `self, other` lies in the equivalence relation specified by `R`
    fn equiv(&self, other: &T) -> bool;
}

/// Values equipped with an equivalence relation specified by the type `R`
trait Equiv<T = Self, R = ()>: PartialEquiv<T, R> {}

/// Values which can be compared modulo an equivalence relation specified by the type `R`
trait PartialOrdEquiv<T = Self, R = ()> {
    /// Compare a value modulo an equivalence relation specified by the type `R`
    fn partial_cmp_equiv(&self, other: &T) -> Option<Ordering>;
}

/// Values which can be compared modulo an equivalence relation specified by the type `R`
trait OrdEquiv<T = Self, R = ()>: PartialOrdEquiv<T, R> {
    /// Compare a value modulo an equivalence relation specified by the type `R`
    fn cmp_equiv(&self, other: &T) -> Ordering;
}

/// Values which can be hashed modulo an equivalence relation specified by the type `R`
trait HashEquiv<R = ()> {
    /// Hash a value modulo an equivalence relation specified by the type `R`
    fn hash_equiv<H: Hasher>(&self, hasher: &mut H);
}

//TODO: wrapper types for `PartialEq`, `Eq`, `PartialOrd`, `Ord`, and `Hash` implementations

// #[derive(Copy, Clone, Equivalence)]
// #[equiv(rel = "()")]
// pub struct Pair {
//     data: u32,
//     #[equiv(ignore)]
//     ignored: u32,
// }

// #[derive(Copy, Clone, Equivalence)]
// #[equiv(rel = "()")]
// pub struct Mapped {
//     #[equiv(map = "|x| x % 5")]
//     data: u32,
//     #[equiv(cmp = "|x, y| i32::abs(x - y) < 10")]
//     other_data: i32,
//     default_data: u64,
//     #[equiv(ignore)]
//     ignored: u128,
//     not_ignored: u32,
// }

impl<R, O, T> PartialEquiv<Box<O>, R> for Box<T>
where
    T: PartialEquiv<O, R>,
{
    #[inline(always)]
    fn equiv(&self, other: &Box<O>) -> bool {
        (**self).equiv(&**other)
    }
}

impl<R, O, T> PartialEquiv<&'_ O, R> for Box<T>
where
    T: PartialEquiv<O, R>,
{
    #[inline(always)]
    fn equiv(&self, other: &&O) -> bool {
        (**self).equiv(&**other)
    }
}

impl<R, O, T> PartialEquiv<&'_ mut O, R> for Box<T>
where
    T: PartialEquiv<O, R>,
{
    #[inline(always)]
    fn equiv(&self, other: &&mut O) -> bool {
        (**self).equiv(&**other)
    }
}

impl<R, O, T> PartialEquiv<Box<O>, R> for &'_ T
where
    T: PartialEquiv<O, R>,
{
    #[inline(always)]
    fn equiv(&self, other: &Box<O>) -> bool {
        (**self).equiv(&**other)
    }
}

impl<R, O, T> PartialEquiv<&'_ O, R> for &'_ T
where
    T: PartialEquiv<O, R>,
{
    #[inline(always)]
    fn equiv(&self, other: &&O) -> bool {
        (**self).equiv(&**other)
    }
}

impl<R, O, T> PartialEquiv<&'_ mut O, R> for &'_ T
where
    T: PartialEquiv<O, R>,
{
    #[inline(always)]
    fn equiv(&self, other: &&mut O) -> bool {
        (**self).equiv(&**other)
    }
}

impl<R, O, T> PartialEquiv<Box<O>, R> for &'_ mut T
where
    T: PartialEquiv<O, R>,
{
    #[inline(always)]
    fn equiv(&self, other: &Box<O>) -> bool {
        (**self).equiv(&**other)
    }
}

impl<R, O, T> PartialEquiv<&'_ O, R> for &'_ mut T
where
    T: PartialEquiv<O, R>,
{
    #[inline(always)]
    fn equiv(&self, other: &&O) -> bool {
        (**self).equiv(&**other)
    }
}

impl<R, O, T> PartialEquiv<&'_ mut O, R> for &'_ mut T
where
    T: PartialEquiv<O, R>,
{
    #[inline(always)]
    fn equiv(&self, other: &&mut O) -> bool {
        (**self).equiv(&**other)
    }
}

impl<R, O, T> PartialEquiv<Vec<O>, R> for Vec<T>
where
    T: PartialEquiv<O, R>,
{
    #[inline]
    fn equiv(&self, other: &Vec<O>) -> bool {
        self.len() == other.len() && self.iter().zip(other.iter()).all(|(l, r)| l.equiv(r))
    }
}

impl<R, O, T> PartialEquiv<&'_ [O], R> for Vec<T>
where
    T: PartialEquiv<O, R>,
{
    #[inline]
    fn equiv(&self, other: &&[O]) -> bool {
        self.len() == other.len() && self.iter().zip(other.iter()).all(|(l, r)| l.equiv(r))
    }
}

impl<R, O, T> PartialEquiv<&'_ mut [O], R> for Vec<T>
where
    T: PartialEquiv<O, R>,
{
    #[inline]
    fn equiv(&self, other: &&mut [O]) -> bool {
        self.len() == other.len() && self.iter().zip(other.iter()).all(|(l, r)| l.equiv(r))
    }
}

impl<R, O, T> PartialEquiv<Vec<O>, R> for &'_ [T]
where
    T: PartialEquiv<O, R>,
{
    #[inline]
    fn equiv(&self, other: &Vec<O>) -> bool {
        self.len() == other.len() && self.iter().zip(other.iter()).all(|(l, r)| l.equiv(r))
    }
}

impl<R, O, T> PartialEquiv<&'_ [O], R> for &'_ [T]
where
    T: PartialEquiv<O, R>,
{
    #[inline]
    fn equiv(&self, other: &&[O]) -> bool {
        self.len() == other.len() && self.iter().zip(other.iter()).all(|(l, r)| l.equiv(r))
    }
}

impl<R, O, T> PartialEquiv<&'_ mut [O], R> for &'_ [T]
where
    T: PartialEquiv<O, R>,
{
    #[inline]
    fn equiv(&self, other: &&mut [O]) -> bool {
        self.len() == other.len() && self.iter().zip(other.iter()).all(|(l, r)| l.equiv(r))
    }
}

impl<R, O, T> PartialEquiv<Vec<O>, R> for &'_ mut [T]
where
    T: PartialEquiv<O, R>,
{
    #[inline]
    fn equiv(&self, other: &Vec<O>) -> bool {
        self.len() == other.len() && self.iter().zip(other.iter()).all(|(l, r)| l.equiv(r))
    }
}

impl<R, O, T> PartialEquiv<&'_ [O], R> for &'_ mut [T]
where
    T: PartialEquiv<O, R>,
{
    #[inline]
    fn equiv(&self, other: &&[O]) -> bool {
        self.len() == other.len() && self.iter().zip(other.iter()).all(|(l, r)| l.equiv(r))
    }
}

impl<R, O, T> PartialEquiv<&'_ mut [O], R> for &'_ mut [T]
where
    T: PartialEquiv<O, R>,
{
    #[inline]
    fn equiv(&self, other: &&mut [O]) -> bool {
        self.len() == other.len() && self.iter().zip(other.iter()).all(|(l, r)| l.equiv(r))
    }
}

impl<R, O, T> Equiv<Box<O>, R> for Box<T> where T: Equiv<O, R> {}

impl<R, O, T> Equiv<&'_ O, R> for Box<T> where T: Equiv<O, R> {}

impl<R, O, T> Equiv<&'_ mut O, R> for Box<T> where T: Equiv<O, R> {}

impl<R, O, T> Equiv<Box<O>, R> for &'_ T where T: Equiv<O, R> {}

impl<R, O, T> Equiv<&'_ O, R> for &'_ T where T: Equiv<O, R> {}

impl<R, O, T> Equiv<&'_ mut O, R> for &'_ T where T: Equiv<O, R> {}

impl<R, O, T> Equiv<Box<O>, R> for &'_ mut T where T: Equiv<O, R> {}

impl<R, O, T> Equiv<&'_ O, R> for &'_ mut T where T: Equiv<O, R> {}

impl<R, O, T> Equiv<&'_ mut O, R> for &'_ mut T where T: Equiv<O, R> {}

impl<R, O, T> Equiv<Vec<O>, R> for Vec<T> where T: Equiv<O, R> {}

impl<R, O, T> Equiv<&'_ [O], R> for Vec<T> where T: Equiv<O, R> {}

impl<R, O, T> Equiv<&'_ mut [O], R> for Vec<T> where T: Equiv<O, R> {}

impl<R, O, T> Equiv<Vec<O>, R> for &'_ [T] where T: Equiv<O, R> {}

impl<R, O, T> Equiv<&'_ [O], R> for &'_ [T] where T: Equiv<O, R> {}

impl<R, O, T> Equiv<&'_ mut [O], R> for &'_ [T] where T: Equiv<O, R> {}

impl<R, O, T> Equiv<Vec<O>, R> for &'_ mut [T] where T: Equiv<O, R> {}

impl<R, O, T> Equiv<&'_ [O], R> for &'_ mut [T] where T: Equiv<O, R> {}

impl<R, O, T> Equiv<&'_ mut [O], R> for &'_ mut [T] where T: Equiv<O, R> {}
